package com.pudonghot.tigon.mybatis;

import lombok.Setter;
import lombok.Getter;
import lombok.ToString;
import java.io.Serializable;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Donghuang
 * @date Mar 25, 2023 16:34:51
 */
@Getter
@Setter
@ToString
@ConfigurationProperties("tigon.mybatis")
public class TigonMyBatisProperties implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * startup check config
     */
    private boolean startupCheck = true;

    /**
     * database quotation mark，for example：MySQL `，Oracle "
     */
    private String quotationMark;

    /**
     * insert DEFAULT instead NULL
     */
    private boolean insertDefaultInsteadNull = true;
}
