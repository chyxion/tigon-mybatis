package com.pudonghot.tigon.mybatis.xmlgen;

/**
 * Xml gen customizer
 *
 * define spring bean implements this interface with annotation @MapperXmlEl
 *
 * @author Donghuang
 * @date Sep 28, 2024 10:50:21
 */
public interface XmlGenCustomizer {
}